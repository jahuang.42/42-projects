#include <stdio.h>
#include <string.h>
#include "libft.h"

int     main(int ac, char **av)
{
    char s[10] = "abcdefg";
    char t[10] = "abcdefg";
    
    printf("%s%s\n", "memset", t);
    ft_memset(s, '1', 5);
    memset(t, '1', 5);


    printf("%s%s\n", "ft_memset", s);
    printf("%s%s\n", "memset", t);
    return (0);
}