/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 03:03:12 by jahuang           #+#    #+#             */
/*   Updated: 2020/11/05 03:03:12 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void    ft_memdel(void **ap)
{
    free(ap);
    ap = 0;
}