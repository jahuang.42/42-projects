#ifndef LIBFT_H
# define LIBFT_H

# include <stddef.h>

void    *ft_memset(void *s, int c, size_t n);

#endif