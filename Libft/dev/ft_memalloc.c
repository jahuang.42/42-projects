/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 03:01:41 by jahuang           #+#    #+#             */
/*   Updated: 2020/11/05 03:01:41 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void    *   ft_memalloc(size_t size)
{
    void    *mem;

    if (size <= 0)
        if (!(mem = (void *)malloc(0)))
            return (0);
    else
        if (!*(mem = (void *)malloc(size * sizeof(void *))))
            return (0);
    return (mem);
}