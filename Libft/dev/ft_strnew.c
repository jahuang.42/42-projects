/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 03:09:08 by jahuang           #+#    #+#             */
/*   Updated: 2020/11/05 03:09:08 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char    *ft_strnew(size_t size)
{
    int i;
    char *result;

    i = 0;
    if (!(result = (char *)malloc(size * sizeof(char))))
        return 0;
    while (i < size)
    {
        result[i] = '\0';
        i++
    }
    result[i] = '\0';
    return (result);
}